# OpenMouseRunner Documentation  

## Intro

## Repositories  
* OMRMouse -> Player/IA
* OMRVisualizer -> See the maze and resolution  
* OMRBehaviours -> AI algo
* OMRPlayground -> Play the maze (engine)  

<br/>  

* OMRRacers -> Examples racers
* OMRMaps -> Examples/Maps generator

## Architectural diagram
![Architectural diagram](./arch_diagram.png)